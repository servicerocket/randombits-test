package it.com.servicerocket.randombits.pageobject;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.upm.pageobjects.OSGiPage;

/**
 * @author Kai Fung
 * @since 1.20140923
 */
public class AddOnOSGIPage extends OSGiPage {

    @ElementBy(cssSelector = "h4.upm-plugin-name")
    PageElement filteringCoreElement;

    public String getFilteringCoreText() {
        return filteringCoreElement.getText();
    }

}
