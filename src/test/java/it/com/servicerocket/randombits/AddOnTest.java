package it.com.servicerocket.randombits;

import it.com.servicerocket.randombits.pageobject.AddOnOSGIPage;

/**
 * @author Kai Fung
 * @since 1.20140923
 */
public class AddOnTest {

    /*
     * To test whether add-on is installed by checking if it's shown in the OSGI page.
     */
    public Boolean isInstalled(AddOnOSGIPage addOnOSGIPage, String addOnName) {
        addOnOSGIPage.filterOsgiAddons(addOnName);
        return addOnOSGIPage.getFilteringCoreText().contains(addOnName);
    }

}
